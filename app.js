const express = require('express');
const app = express();
const db = require('./db');
const Usuario = require('./usuario');
const fcm = require('./fcm');

app.use(express.json());

app.get('/', (req, res) => {
    res.redirect('/usuarios');
})

// Obtiene todos los usuarios
app.get('/usuarios', (req, res) => {
    Usuario.find({}, '-_id -__v', (err, usuarios) => {
        res.status(200).json({ usuarios: usuarios });
    });

});

// Obtiene el usuario identificado por email
app.get('/usuarios/:email', (req, res) => {
    Usuario.findOne({ email: req.params.email }, '-_id -__v', (err, usuario) => {
        console.log(req.params.email);
        if (err) {
            return res.status(500).json({ mensaje: "Error interno del servidor. Intente nuevamente" });
        }

        if (!usuario) {
            return res.status(404).json({ mensaje: 'Usuario no encontrado' });
        }

        res.status(200).json(usuario);
    });
});

// Actualiza el usuario identificado por nombre
app.put('/usuarios/:email', (req, res) => {
    Usuario.findOneAndUpdate({ email: req.params.email }, req.body, (err, usuario) => {
        if (err) {
            return res.status(500).json({ mensaje: "Error interno del servidor. Intente nuevamente" });
        }

        if (!usuario) {
            return res.status(404).json({ mensaje: 'Usuario no encontrado' });
        }

        fcm.enviarNotificacion({
            title: 'Usuario actualizado', message: 'Se han actualizado los datos de un usuario',
            nombre: usuario.nombre,
            apellido: usuario.apellido,
            empresa: usuario.empresa,
            direccion: usuario.direccion,
            edad: usuario.edad,
            email: usuario.email,
            accion: 'actualizar',
        });
        res.status(200).json({ mensaje: 'Usuario actualizado con exito' });
    });
});

// Crea un nuevo usuario
app.post('/usuarios', (req, res) => {
    Usuario.create(req.body, (err, usuario) => {
        if (err) {
            return res.status(500).json({ mensaje: "Error interno del servidor. Intente nuevamente" });
        }

        if (!usuario) {
            return res.status(400).json({ mensaje: 'Hubo un error al crear el usuario. Verifique la peticion e intente nuevamente' });
        }

        fcm.enviarNotificacion({
            title: 'Usuario creado', message: 'Se ha creado un nuevo usuario',
            nombre: usuario.nombre,
            apellido: usuario.apellido,
            empresa: usuario.empresa,
            direccion: usuario.direccion,
            edad: usuario.edad,
            email: usuario.email,
            accion: 'crear',
        });
        res.status(201).json({ mensaje: 'Usuario creado con exito' });
    });
});

// Elimina el usuario identificado por nombre
app.delete('/usuarios/:email', (req, res) => {
    Usuario.findOneAndDelete({ email: req.params.email }, (err, usuario) => {
        if (err) {
            return res.status(500).json({ mensaje: "Error interno del servidor. Intente nuevamente" });
        }

        if (!usuario) {
            return res.status(404).json({ mensaje: 'Usuario no encontrado' });
        }

        fcm.enviarNotificacion({
            title: 'Usuario eliminado', message: 'Se ha eliminado un usuario',
            nombre: usuario.nombre,
            apellido: usuario.apellido,
            empresa: usuario.empresa,
            direccion: usuario.direccion,
            edad: usuario.edad,
            email: usuario.email,
            accion: 'eliminar',
        });

        res.status(200).json({ mensaje: 'Usuario eliminado con exito' });
    });
});

app.get('/prueba', (req, res) => {
    fcm.enviarNotificacion({
        title: 'Notificacion de prueba', message: 'Mensaje de prueba',
        nombre: 'TEST',
        apellido: 'TEST',
        empresa: 'TEST',
        direccion: 'TEST',
        edad: '21',
        email: 'test@gmail.com',
        accion: 'probar',
    });
    res.sendStatus(200);
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Escuchando en puerto ${PORT}`));