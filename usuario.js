const mongoose = require('mongoose');

const EsquemaUsuario = new mongoose.Schema({
    nombre: String,
    apellido: String,
    empresa: String,
    direccion: String,
    edad: String,
    email: String,
});

const Usuario = mongoose.model('Usuario', EsquemaUsuario);
module.exports = Usuario;