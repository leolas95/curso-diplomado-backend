const request = require('request');

function enviarNotificacion(data) {
    request.post('https://fcm.googleapis.com/fcm/send',
        {
            json: {
                "to": "edDQl1OplJQ:APA91bHHIZ9YbYgiJqSJhU3geuu1NfGKWDN7N1QPGqv69IudKUzpX810LDshHT1owFvjSYrEW3eT5bdzlwWqSmM4YlVIy2FZNa_H6iXLrT_Q9apSfNhDIZSijV5HiOQVUWTvBBfFMKBU",
                "data": data,
                "priority": 10
            },
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'key=AAAALGZnF8k:APA91bE4g5Rq9-n0Cvy9kWbG-YYSwuWl3QAWhuHQtoADU1ZSVqX3p3OURrb_InHe0vXhQjXYwh_HLqGib-GHRvucJfY4Cd9IF_Wvuaedtb03CtJ0jPVUUOqxHKYtO_wWWfco_81QSKdA'
            }
        }
    );
}

module.exports = {
    enviarNotificacion: enviarNotificacion,
};